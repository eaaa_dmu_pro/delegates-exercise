﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesExercise.Model
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void PrintFullNameLastNameFirst()
        {
            Console.WriteLine($"{LastName}, {FirstName}");
        }

        public void PrintFullNameAllCaps()
        {
            Console.WriteLine($"{LastName.ToUpper()}, {FirstName.ToUpper()}");
        }

        public void PrintFullNameLowerCase()
        {
            Console.WriteLine($"{LastName.ToLower()}, {FirstName.ToLower()}");
        }

        public void PrintShortName()
        {
            Console.WriteLine($"{FirstName.Substring(0, 1)}. {LastName}");
        }

    }
}
